#! /usr/bin/env perl

use strict;
use warnings;
use Encode;
use utf8;
 
use lib('../lib');
use FakeFramework;

my $com = FakeFramework->new;
my $form = $com->get_form;

print <<HEAD;
Content-Type: text/html; charset=utf-8

<html lang="en">
    <head>
        <title>fixed encoding bug</title>
    </head>
    <body>
        <form action="fixed_encoding_bug.cgi">
            <h1>fixed encoding bug</h1>
            <input type="textarea" name="box1"/>
            <table>
HEAD

foreach my $key (keys(%$form)) {
    my $value = decode('utf8', $form->{$key}, Encode::FB_CROAK);
    $value =~ s/\%/％/g;
    my $encoded_value = encode('utf8', $value, Encode::FB_CROAK);
    print "<tr><td>$key=$encoded_value</td></tr>\n";
}

print <<TAIL;
            </table>
            <input type="submit"/>
        </form>
    </body>
</html>
TAIL
