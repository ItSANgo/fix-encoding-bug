#! /usr/bin/env perl

use strict;
use warnings;
use utf8;
 
use lib('../lib');
use FakeFramework;

my $com = FakeFramework->new;
my $form = $com->get_form;

print <<HEAD;
Content-Type: text/html; charset=utf-8

<html lang="en">
    <head>
        <title>encoding bug</title>
    </head>
    <body>
        <form action="encoding_bug.cgi">
            <h1>encoding bug</h1>
            <input type="textarea" name="box1"/>
            <table>
HEAD

foreach my $key (keys(%$form)) {
    my $value = $form->{$key};
    $value =~ s/\%/％/g;
    print "<tr><td>$key=$value</td></tr>\n";
}

print <<TAIL;
            </table>
            <input type="submit"/>
        </form>
    </body>
</html>
TAIL
