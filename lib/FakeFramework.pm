package FakeFramework;

use strict;
use warnings;

use CGI::Lite;
our @ISA = qw(CGI::Lite);

 sub new {
    my $class = shift;
    my $self = {};
    bless $self, $class;
    return $self;
  }

sub get_form() {
    my $self = shift;
    return $self->parse_form_data;
}
